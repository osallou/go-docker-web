import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.md')) as f:
    CHANGES = f.read()

requires = [
    'PyYAML',
    'pymongo',
    'redis',
    'yapsy',
    'iso8601',
    'gunicorn',
    'gevent',
    'pyramid',
    'pyramid_beaker',
    'pyramid-mako',
    'pyramid_debugtoolbar',
    'waitress',
    'sphinx',
    'sphinxcontrib-httpdomain',
    'mock',
    'PyJWT',
    'cryptography',
    'jsonschema',
    'urllib3',
    'prometheus_client>=0.0.18',
    'python-etcd',
    'velruse',
    'humanfriendly'
    ]

test_requires = [
   'WebTest',
   'nose'
   ]

setup(name='godweb',
      version='1.3.18',
      description='GO-Docker Web interface',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='Olivier Sallou',
      author_email='olivier.sallou@irisa.fr',
      url='https://bitbucket.org/osallou/go-docker',
      keywords='web pyramid pylons godocker',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=test_requires,
      test_suite="test",
      entry_points="""\
      [paste.app_factory]
      main = godweb:main
      """,
      )
